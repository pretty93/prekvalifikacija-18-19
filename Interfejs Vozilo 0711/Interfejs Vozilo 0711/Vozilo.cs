﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfejs_Vozilo_0711
{
    public class Vozilo
    {
        public bool stanje;
        public bool stanjeVrata;
        public int brzina;
        public int potrosnja;
        public int maxBrzina;

        public Vozilo(bool stanje, bool stanjeVrata, int brzina, int potrosnja, int maxBrzina)
        {
            this.stanje = stanje;
            this.stanjeVrata = stanjeVrata;
            this.brzina = brzina;
            this.potrosnja = potrosnja;
            this.maxBrzina = maxBrzina;
        }

        public void otvori()
        {
            this.stanjeVrata = true;
        }

        public void zatvori()
        {
            this.stanjeVrata = false;
        }

        public void prikazi()
        {
            Console.WriteLine("Prikazi sve");
        }

        public void ubrzaj()
        {
            this.brzina++;
        }

        public void uspori()
        {
            this.brzina--;
        }

        public void prikaziManjuPotrosnju()
        {
            if()
        }
    }
}
