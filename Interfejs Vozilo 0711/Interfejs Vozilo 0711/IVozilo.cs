﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfejs_Vozilo_0711
{
    public interface IVozilo
    {
        void upali();
        void ugasi();
        void otvori();
        void zatvori();
        void ubrzaj();
        void uspori();
        void prikazi();
        void prikaziManjuPotrosnju();
        void prikaziVecuMaksimalnuBrzinu(Vozilo a);
    }
}
