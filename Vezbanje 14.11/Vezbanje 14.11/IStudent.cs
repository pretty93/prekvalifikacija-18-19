﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vezbanje_14._11
{
    public interface IStudent
    {
        void dodajOcenu();
        void ucitaj();
        void azuriraj();
        void prikaziOcene();
        void prikazi();
        Student vratiVisePolozenihIspita(student s);
        Student vratiVecuProsecnu(student s);

    }
}
