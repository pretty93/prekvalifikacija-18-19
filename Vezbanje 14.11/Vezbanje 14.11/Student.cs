﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vezbanje_14._11
{
    public class Student : IStudent
    {
        public string ime;
        public string prezime;
        int[] ocene = new int[10];
        int brojac = 0;

        public Student()
        {

        }

        public void azuriraj()
        {
            Console.WriteLine("Unesite novo ime za studenta: ");
            ime = Console.ReadLine();
            Console.WriteLine("Unesite novo prezime za studenta: ");
            prezime = Console.ReadLine();
        }

        public void dodajOcenu()
        {
            Console.WriteLine("Unesite ocenu sa ispita: ");
            ocene[brojac] = int.Parse(Console.ReadLine());
            brojac++;
        }

        public void prikazi()
        {
            using (StreamWriter file = new StreamWriter("oceneprikaz.txt"))
            {
                for (int i = 0; i < brojac; i++)
                {
                    file.WriteLine(ocene[i]);
                }
            }
        }

        public void prikaziOcene()
        {
            int pom;
            for (int i = 1; i < brojac; i++)
            {
                for (int j = brojac - 1; j >= i; j--)
                {
                    if (ocene [j - 1] > ocene[j])
                    {
                        pom = ocene[j];
                        ocene[j] = ocene[j - 1];
                        ocene[j - 1] = pom;
                    }
                }
            }
            for (int i = 0; i < brojac; i++)
            {
                Console.WriteLine(ocene[i]);
            }
        }

        public void ucitaj()
        {
            string linija = " ";
            using (StreamReader sr = new StreamReader("ocene.txt"))
            {
                while ((linija = sr.ReadLine()) !=null)
                {

                }
            }
    }
}
