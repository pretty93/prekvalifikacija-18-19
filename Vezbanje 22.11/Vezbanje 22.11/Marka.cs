﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vezbanje_22._11
{
    public class Marka
    {
        private string imeMarke;
        private string imeZemljeStampanja;
        private int godinaStampanja;

        public Marka(string imeMarke, string imeZemljeStampanja, int godinaStampanja)
        {
            this.imeMarke = imeMarke;
            this.imeZemljeStampanja = imeZemljeStampanja;
            this.godinaStampanja = godinaStampanja;
        }

        public override string ToString()
        {
            return this.imeMarke + "" + this.imeZemljeStampanja + "";
        }

        
    }
}
