﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vezbanje_22._11
{
    class Slika
    {
        private int datum;
        private string mestoSlikanja;
        public int godinaStampanja;

        public Slika(int datum, string mestoSlikanja, int godinaStampanja)
        {
            this.datum = datum;
            this.mestoSlikanja = mestoSlikanja;
            this.godinaStampanja = godinaStampanja;
        }

        public override string ToString()
        {
            return this.mestoSlikanja + "";
        }
    }
}
