﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vezbanje_26._11_3
{
    public class Program
    {
        static void Main(string[] args)
        {
            List<Fudbaleri> listaFudbaleri = new List<Fudbaleri>();
            Fudbaleri Barnes = new Fudbaleri(9, "John", "Barnes", 30);
            Fudbaleri Smicer = new Fudbaleri(11, "Vladimir", "Smicer", 25);
            Fudbaleri Suarez = new Fudbaleri(15, "Luis", "Suarez", 40);

            listaFudbaleri.Add(Barnes);
            listaFudbaleri.Add(Smicer);
            listaFudbaleri.Add(Suarez);

            var upit = from igraci in listaFudbaleri
                       where igraci.brojGolova > 38
                       orderby igraci ascending
                       select igraci;

            foreach (var igraci in upit)
            {
                Console.Write(igraci + "Igrac sa najvise golova je");
            }
        }
    }
}
