﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vezbanje_26._11_3
{
    public class Fudbaleri
    {
        public int brojDresa;
        public string ime;
        public string prezime;
        public int brojGolova;

        public Fudbaleri (int brojDresa, string ime, string prezime, int brojGolova)
        {
            this.brojDresa = brojDresa;
            this.ime = ime;
            this.prezime = prezime;
            this.brojGolova = brojGolova;
        }

    }
}
