﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace VozniPark20._11
{
    public interface IVozilo
    {
        void upali();
        void ugasi();
        void otvori();
        void zatvori();
        void ubrzaj();
        void uspori();
        void prikazi();
        Vozilo prikaziManjuPotrosnju(Vozilo a);
    }
}
