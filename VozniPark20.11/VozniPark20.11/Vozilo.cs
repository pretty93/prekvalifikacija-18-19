﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace VozniPark20._11
{
    public class Vozilo : IVozilo
    {
        public bool stanjeVozila;
        public bool stanjeVrata;
        public int brzina;
        public float potrosnja;

        public Vozilo(bool stanjeVozila, bool stanjeVrata, int brzina, float potrosnja)
        {
            this.stanjeVozila = stanjeVozila;
            this.stanjeVrata = stanjeVrata;
            this.brzina = brzina;
            this.potrosnja = potrosnja;
        }

        public void otvori()
        {
            this.stanjeVrata = true;
        }

        public void prikazi()
        {
            Console.WriteLine("Stanje vrata su: " + this.stanjeVrata);
            Console.WriteLine("Stanje Vozila je: " + this.stanjeVozila);
            Console.WriteLine("Trenutna brzina kojom se vozilo krece: " + this.brzina);
            Console.WriteLine("Potrosnja vozila je: " + this.potrosnja);
        }

        public Vozilo prikaziManjuPotrosnju(Vozilo a)
        {
            return this.potrosnja < a.potrosnja ? this : a;
        }

        public void ubrzaj()
        {
            this.brzina += 20;
        }

        public void ugasi()
        {
            this.stanjeVozila = false;
        }

        public void upali()
        {
            this.stanjeVozila = true;
        }

        public void uspori()
        {
            this.brzina -= 20;
        }

        public void zatvori()
        {
            this.stanjeVrata = false;
        }
    }
}
