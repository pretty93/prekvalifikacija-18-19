﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace VozniPark20._11
{
    public class Program
    {
        static void Main(string[] args)
        {
            VozniPark park1 = new VozniPark();
            Vozilo golf = new Vozilo(true, false, 50, 4);
            Vozilo audi = new Vozilo(true, false, 65, 5);
            Vozilo bmw = new Vozilo(true, false, 57, 3);
            Vozilo lada = new Vozilo(true, true, 22, 11);
            Vozilo jugo = new Vozilo(true, false, 33, 11);

            park1.popuniMatricu(golf);
            park1.popuniMatricu(audi);
            park1.popuniMatricu(bmw);
            park1.popuniMatricu(lada);
            park1.popuniMatricu(jugo);

            park1.prikaziMatricu();
            park1.upisiUFajl();
        }
    }
}
