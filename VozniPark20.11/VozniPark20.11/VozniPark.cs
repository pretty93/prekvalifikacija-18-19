﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace VozniPark20._11
{
    public class VozniPark
    {
        private Vozilo[,] vozniPark = new Vozilo[2, 2];
        private int i;
        private int j;

        public VozniPark()
        {

        }

        public VozniPark(Vozilo[,] vozniPark)
        {

        }

        public void prikaziMatricu()
        {
            for (int i = 0; i < this.vozniPark.GetLength(0); i++)
            {
                for (int j = 0; j < this.vozniPark.GetLength(1); j++)
                {
                    Console.WriteLine(this.vozniPark[i,j].brzina);
                }
            }
        }

        public void popuniVozniPark()
        {

        }

        public void popuniMatricu(Vozilo v1)
        {
            var iDuzina = vozniPark.GetLength(0);
            var jDuzina = vozniPark.GetLength(1);

            if (j < jDuzina && i <= iDuzina)
            {
                this.vozniPark[i, j] = new Vozilo(v1.stanjeVozila, v1.stanjeVrata, v1.brzina, v1.potrosnja);
                j++;
            }
            else if( ++i < iDuzina)
            {
                j = 0;
                this.vozniPark[i, j] = new Vozilo(v1.stanjeVozila, v1.stanjeVrata, v1.brzina, v1.potrosnja);
                j++;
            }
            else
            {
                Console.WriteLine("Matrica je popunjena");
            }
        }

        public void upisiUFajl()
        {
            using (StreamWriter sw = new StreamWriter("vozniPark.txt"))
            {
                for (int i = 0; i < this.vozniPark.GetLength(0); i++)
                {
                    for (j = 0; j < this.vozniPark.GetLength(1); j++)
                    {
                        sw.WriteLine(this.vozniPark[i, j].brzina);
                    }
                }
            }
        }
    }
}
