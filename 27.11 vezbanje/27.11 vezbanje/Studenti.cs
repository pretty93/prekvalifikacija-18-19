﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _27._11_vezbanje
{
    public class Studenti
    {
        public int BrojStudenta { get; set; }
        public string ImeStudenta { get; set; }
        public string PrezimeStudenta { get; set; }
        public int StarostStudenta { get; set; }
        public int ProsekStudenta { get; set; }
    }
}
