﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace _27._11_vezbanje
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Studenti> listaStudenata = new List<Studenti>();


            Studenti s1 = new Studenti
            {
                BrojStudenta = 1,
                ImeStudenta = "Aleksandar",
                PrezimeStudenta = "Jovanovic",
                StarostStudenta = 25,
                ProsekStudenta = 3,
            };

            Studenti s2 = new Studenti
            {
                BrojStudenta = 2,
                ImeStudenta = "Lazar",
                PrezimeStudenta = "Zubic",
                StarostStudenta = 26,
                ProsekStudenta = 4,
            };

            Studenti s3 = new Studenti
            {
                BrojStudenta = 3,
                ImeStudenta = "Stefan",
                PrezimeStudenta = "Petrov",
                StarostStudenta = 27,
                ProsekStudenta = 5,
            };

            listaStudenata.Add(s1);
            listaStudenata.Add(s2);
            listaStudenata.Add(s3);

            var upit = from Studenti in listaStudenata
                       where Studenti.ProsekStudenta > 3
                       orderby Studenti.ProsekStudenta descending
                       select Studenti;

            foreach(Studenti s in upit)
            {
                Console.WriteLine(s.ImeStudenta + " " + s.PrezimeStudenta);
            }

            using (StreamWriter Upis = new StreamWriter("Studenti.txt"))
            {
                foreach(Studenti s in upit)
                {
                    Upis.WriteLine(s.ImeStudenta + "," + s.PrezimeStudenta);
                }
            }
        }
    }
}
