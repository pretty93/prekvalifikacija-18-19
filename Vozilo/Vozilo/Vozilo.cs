﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vozilo
{
    public class Vozilo
    {
        public int BrojTockova;
        public int BrojSedista;
        public int brzina;

        public Vozilo(int BrojTockova, int BrojSedista, int brzina)
        {
            this.BrojTockova = BrojTockova;
            this.BrojSedista = BrojSedista;
            this.brzina = brzina;
        }



        public int getBrojTockova()
        {
            return this.BrojTockova;
        }
        public int getBrojSedista()
        {
            return this.BrojSedista;
        }
        public virtual int ubrzaj()
        {
            return this.brzina += 1;
        }
        public virtual int uspori()
        {
            return this.brzina -= 1;
        }
    }
}
