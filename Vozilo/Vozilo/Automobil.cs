﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vozilo
{
    public class Automobil : Vozilo
    {

        public Automobil (int BrojTockova, int BrojSedista, int brzina) : base(BrojTockova, BrojSedista, brzina)
        {

        }

        public override int ubrzaj()
        {
            return base.brzina += 10;
        }
        public override int uspori()
        {
            return base.brzina -= 10;
        }
        
    }
    
}
