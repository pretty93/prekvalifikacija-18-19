﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vozilo
{
    public class Program
    {
        static void Main(string[] args)
        {
            Vozilo[] nizVozilo = new Vozilo[5];
            Vozilo bmx = new Bicikl(2, 1, 30);
            Vozilo ponika = new Bicikl(2, 1, 35);
            Vozilo audi = new Automobil(4, 2, 250);
            Vozilo bmw = new Automobil(4, 5, 200);
            Vozilo yugo = new Automobil(4, 5, 150);

            nizVozilo[0] = bmx;
            nizVozilo[1] = ponika;
            nizVozilo[2] = audi;
            nizVozilo[3] = bmw;
            nizVozilo[4] = yugo;

            for (int i = 0; i < nizVozilo.Length; i++)
            {
                Console.WriteLine("Broj tockova vozila su: " + nizVozilo[i].BrojTockova);
                Console.WriteLine("Broj sedista vozila su " + nizVozilo[i].BrojSedista);
                Console.WriteLine("Brzina vozila je " + nizVozilo[i].brzina);
                Console.WriteLine("Nakon ubrzanja, brzina vozila je: " + nizVozilo[i].ubrzaj());
                Console.WriteLine("Nakon usporavanja, brzina vozila je: " + nizVozilo[i].uspori());
            }
        }


    }
}
