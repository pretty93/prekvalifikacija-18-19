﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vozilo
{
    public class Bicikl : Vozilo
    {
        public Bicikl(int BrojTockova, int BrojSedista, int brzina) : base(BrojTockova, BrojSedista, brzina)
        {

        }

        public override int ubrzaj()
        {
            return this.brzina += 5;
        }
        public override int uspori()
        {
            return this.brzina -= 5;
        }





    }
        
}
