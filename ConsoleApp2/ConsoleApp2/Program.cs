﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleApp2.Kalkulatorvezbanje;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Kalkulatorvezbanje.CalculatorSoapClient client = new Kalkulatorvezbanje.CalculatorSoapClient();
            var promenljiva = client.Divide(99, 3);
            Console.WriteLine(promenljiva);
        }
    }
}
