﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vezbanje15._11_2
{
    public interface IStudent
    {
        void dodajOcenu();
        void ucitaj();
        void azuriraj();
        void prikaziOcene();
        void prikazi();
        Student vratiVisePolozenih(Student s);
    }
}
