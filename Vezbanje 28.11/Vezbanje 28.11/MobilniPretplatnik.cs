﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vezbanje_28._11
{
    public class MobilniPretplatnik
    {
        public string NazivOperatera { get; set; }
        public string BrojPretplatnika { get; set; }
        public string ImePretplatnika { get; set; }
        public string PrezimePretplatnika { get; set; }
        public string AdresaPretplatnika { get; set; }


    }
}
