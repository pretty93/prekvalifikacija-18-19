﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vezbanje_28._11
{
    class Program
    {
        static void Main(string[] args)
        {
            List<MobilniPretplatnik> listaMobilnihPretplatnika = new List<MobilniPretplatnik>();

            MobilniPretplatnik p1 = new MobilniPretplatnik
            {
                NazivOperatera = "Telenor",
                BrojPretplatnika = "018245488",
                ImePretplatnika = "Aleksandar",
                PrezimePretplatnika = "Peric",
                AdresaPretplatnika = "Bulevar Nemanjica 29/4",
            };

            MobilniPretplatnik p2 = new MobilniPretplatnik
            {
                NazivOperatera = "Mts",
                BrojPretplatnika = "018249944",
                ImePretplatnika = "Dusan",
                PrezimePretplatnika = "Milic",
                AdresaPretplatnika = "Nusiceva 31",
            };

            MobilniPretplatnik p3 = new MobilniPretplatnik
            {
                NazivOperatera = "Vip",
                BrojPretplatnika = "018345654",
                ImePretplatnika = "Katarina",
                PrezimePretplatnika = "Sipic",
                AdresaPretplatnika = "Dusanova 20",
            };

            listaMobilnihPretplatnika.Add(p1);
            listaMobilnihPretplatnika.Add(p2);
            listaMobilnihPretplatnika.Add(p3);


            List<Racuni> listaRacuna = new List<Racuni>();

            Racuni r1 = new Racuni
            {
                BrojPretplatnika = "018245488",
                StatusRacuna = true,
                IznosRacuna = 928,
                MesecRacuna = "Avgust",
            };

            Racuni r2 = new Racuni
            {
                BrojPretplatnika = "018249944",
                StatusRacuna = false,
                IznosRacuna = 1120,
                MesecRacuna = "Septembar",
            };

            Racuni r3 = new Racuni
            {
                BrojPretplatnika = "018249944",
                StatusRacuna = true,
                IznosRacuna = 888,
                MesecRacuna = "Jul",
            };
            listaRacuna.Add(r1);
            listaRacuna.Add(r2);
            listaRacuna.Add(r3);
        }
    }
}
