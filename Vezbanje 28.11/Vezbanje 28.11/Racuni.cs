﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vezbanje_28._11
{
    public class Racuni
    {
        public string BrojPretplatnika { get; set; }
        public bool StatusRacuna { get; set; }
        private int IznosRacuna { get; set; }
        private string MesecRacuna { get; set; }

        public Racuni()
        {

        }

        public void setIznosRacuna(int iznos)
        {
            this.IznosRacuna = iznos;
        }

        public int getIznosRacuna()
        {
            return this.IznosRacuna;
        }

        public string getMesecRacuna()
        {
            return this.MesecRacuna;
        }

        
    }
}
