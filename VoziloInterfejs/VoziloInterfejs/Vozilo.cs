﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoziloInterfejs
{
    public class Vozilo : IVozilo
    {
        public bool stanje;
        public bool stanjeVrata;
        public int brzina;
        public int potrosnja;
        public int maxBrzina;

        public Vozilo(bool stanje, bool stanjeVrata, int brzina, int potrosnja, int maxBrzina)
        {
            this.stanje = stanje;
            this.stanjeVrata = stanjeVrata;
            this.brzina = brzina;
            this.potrosnja = potrosnja;
            this.maxBrzina = maxBrzina;
        }

        public void otvori()
        {
            this.stanjeVrata = true;
    
        }

        public void prikazi()
        {
            Console.WriteLine("Prikazi sve");
        }
        public Vozilo prikaziManjuPotrosnju(Vozilo jugo)
        {
            if (this.potrosnja < jugo.potrosnja)
            {
                return this;
            }
            else
            {
                return jugo;
            }
        }

        public Vozilo prikaziVecuMaksimalnuBrzinu(Vozilo jugo)
        {
            if (this.maxBrzina < jugo.maxBrzina)
            {
                return this;
            }
            else
            {
                return jugo;
            }
        }
                public void ubrzaj()
                {
                    this.brzina++;
                }
                public void uspori()
                {
                    this.brzina--;
                }
                public void upali()
                {
                    this.stanje = true;
                }
                public void ugasi()
                {
                    this.stanje = false;
                }
                public void zatvori()
                {
                    this.stanjeVrata = false;
                }
            
        
    }
}
