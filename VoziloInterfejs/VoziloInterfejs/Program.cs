﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoziloInterfejs
{
    class Program
    {
        static void Main(string[] args)
        {
            Vozilo jugo = new Vozilo(false, false, 50, 5, 150);
            Vozilo golf = new Vozilo(false, false, 70, 4, 190);

            Vozilo voziloKojeMiJeVratilaFunkcija = golf.prikaziManjuPotrosnju(jugo);
        }
        
    }
}
