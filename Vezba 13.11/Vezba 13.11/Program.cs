﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vezba_13._11
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] brojevi = new int[] { 1, 2, 3, 4, 5 };

            using (StreamWriter sw = new StreamWriter("primer.txt"))
            {
                for (int i = 0; i < brojevi.Length; i++)
                {
                    sw.WriteLine(i);
                }
            }
        }
    }
}
