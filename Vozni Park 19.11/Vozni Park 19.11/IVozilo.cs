﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vozni_Park_19._11
{
    public interface IVozilo
    {
        public void upali();
        public void ugasi();
        public void otvori();
        public void zatvori();
        public void ubrzaj();
        public void uspori();
        public void prikazi();
        Automobil prikaziManjuPotrosnju;
    }
}
