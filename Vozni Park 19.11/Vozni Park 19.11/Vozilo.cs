﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vozni_Park_19._11
{
    public class Vozilo : IVozilo
    {
        public bool stanje;
        public bool stanjeVrata;
        public int brzina;
        public int potrosnja;
        public int maxBrzina;

        public Vozilo (bool stanje, bool stanjeVrata, int brzina, int potrosnja, int MaxBrzina)
        {
            this.stanje = true;
            this.stanjeVrata = false;
            this.brzina = brzina;
            this.potrosnja = potrosnja;
            this.maxBrzina = MaxBrzina;

        }

        public void otvori()
        {
            this.stanjeVrata = true;
        }

        public void prikazi()
        {
            Console.WriteLine("Prikazi sve");
        }

        public void ubrzaj()
        {
            this.brzina++;
        }

        public void ugasi()
        {
            this.stanje = false;
        }

        public void upali()
        {
            this.stanje = true;
        }

        public void uspori()
        {
            this.brzina--;
        }

        public void zatvori()
        {
            this.stanjeVrata = false;
        }
    }
}
